
const form = document.querySelector('form'),
      inputName = document.querySelector('.form__name'),
      inputLastName = document.querySelector('.form__lastname'),
      inputPhone = document.querySelector('.form__phone'),
      inputEmail = document.querySelector('.form__email'),
      inputSubmit = document.querySelector('.form__submit'),
      errors = document.querySelectorAll('.error'),
      fields = document.querySelectorAll('.form__field');

      const removeErrorValidation = () => {
        let errors = form.querySelectorAll('.error')
        for (let i = 0; i < errors.length; i++) {
          errors[i].remove()
        }
      }

      const checkFieldsEmpty = () => {
        if(!inputName.value || !inputLastName.value || !inputPhone.value || !inputEmail.value){
          for(let i = 0; i < fields.length;i++){
            let error = document.createElement('div');
            error.className = "error";
            error.innerHTML = 'заполните поля';
            fields[i].insertAdjacentElement('afterbegin',error);
          }
         }else{
           console.log(`
            Имя: ${inputName.value} Фамилия: ${inputLastName.value}
            Телефон: ${inputPhone.value} "Email: ${inputEmail.value}
           `)
         }
      }

      form.addEventListener('submit',(e)=>{
        e.preventDefault();
        removeErrorValidation();
        checkFieldsEmpty();
      })